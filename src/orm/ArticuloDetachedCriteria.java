/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ArticuloDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression idArticulo;
	public final StringExpression nombreArticulo;
	public final StringExpression descripcionArticulo;
	public final StringExpression precioCompra;
	public final StringExpression valorAprox;
	public final StringExpression idProveedor;
	public final StringExpression nombreProveedor;
	public final StringExpression direccionProveedor;
	public final StringExpression activo;
	public final StringExpression idBodega;
	public final StringExpression nombreBodega;
	
	public ArticuloDetachedCriteria() {
		super(orm.Articulo.class, orm.ArticuloCriteria.class);
		idArticulo = new IntegerExpression("idArticulo", this.getDetachedCriteria());
		nombreArticulo = new StringExpression("nombreArticulo", this.getDetachedCriteria());
		descripcionArticulo = new StringExpression("descripcionArticulo", this.getDetachedCriteria());
		precioCompra = new StringExpression("precioCompra", this.getDetachedCriteria());
		valorAprox = new StringExpression("valorAprox", this.getDetachedCriteria());
		idProveedor = new StringExpression("idProveedor", this.getDetachedCriteria());
		nombreProveedor = new StringExpression("nombreProveedor", this.getDetachedCriteria());
		direccionProveedor = new StringExpression("direccionProveedor", this.getDetachedCriteria());
		activo = new StringExpression("activo", this.getDetachedCriteria());
		idBodega = new StringExpression("idBodega", this.getDetachedCriteria());
		nombreBodega = new StringExpression("nombreBodega", this.getDetachedCriteria());
	}
	
	public ArticuloDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.ArticuloCriteria.class);
		idArticulo = new IntegerExpression("idArticulo", this.getDetachedCriteria());
		nombreArticulo = new StringExpression("nombreArticulo", this.getDetachedCriteria());
		descripcionArticulo = new StringExpression("descripcionArticulo", this.getDetachedCriteria());
		precioCompra = new StringExpression("precioCompra", this.getDetachedCriteria());
		valorAprox = new StringExpression("valorAprox", this.getDetachedCriteria());
		idProveedor = new StringExpression("idProveedor", this.getDetachedCriteria());
		nombreProveedor = new StringExpression("nombreProveedor", this.getDetachedCriteria());
		direccionProveedor = new StringExpression("direccionProveedor", this.getDetachedCriteria());
		activo = new StringExpression("activo", this.getDetachedCriteria());
		idBodega = new StringExpression("idBodega", this.getDetachedCriteria());
		nombreBodega = new StringExpression("nombreBodega", this.getDetachedCriteria());
	}
	
	public Articulo uniqueArticulo(PersistentSession session) {
		return (Articulo) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Articulo[] listArticulo(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Articulo[]) list.toArray(new Articulo[list.size()]);
	}
}

