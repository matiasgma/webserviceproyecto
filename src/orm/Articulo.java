/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Articulo {
	public Articulo() {
	}
	
	private int idArticulo;
	
	private String nombreArticulo;
	
	private String descripcionArticulo;
	
	private String precioCompra;
	
	private String valorAprox;
	
	private String idProveedor;
	
	private String nombreProveedor;
	
	private String direccionProveedor;
	
	private String activo;
	
	private String idBodega;
	
	private String nombreBodega;
	
	/**
	 * Clave Primaria
	 */
	private void setIdArticulo(int value) {
		this.idArticulo = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getIdArticulo() {
		return idArticulo;
	}
	
	public int getORMID() {
		return getIdArticulo();
	}
	
	/**
	 * Nombre Articulo
	 */
	public void setNombreArticulo(String value) {
		this.nombreArticulo = value;
	}
	
	/**
	 * Nombre Articulo
	 */
	public String getNombreArticulo() {
		return nombreArticulo;
	}
	
	/**
	 * Descripcion Articulo
	 */
	public void setDescripcionArticulo(String value) {
		this.descripcionArticulo = value;
	}
	
	/**
	 * Descripcion Articulo
	 */
	public String getDescripcionArticulo() {
		return descripcionArticulo;
	}
	
	/**
	 * Precio Compra
	 */
	public void setPrecioCompra(String value) {
		this.precioCompra = value;
	}
	
	/**
	 * Precio Compra
	 */
	public String getPrecioCompra() {
		return precioCompra;
	}
	
	/**
	 * Valor Aproximado
	 */
	public void setValorAprox(String value) {
		this.valorAprox = value;
	}
	
	/**
	 * Valor Aproximado
	 */
	public String getValorAprox() {
		return valorAprox;
	}
	
	/**
	 * ID Proveedor
	 */
	public void setIdProveedor(String value) {
		this.idProveedor = value;
	}
	
	/**
	 * ID Proveedor
	 */
	public String getIdProveedor() {
		return idProveedor;
	}
	
	/**
	 * Nombre Proveedor
	 */
	public void setNombreProveedor(String value) {
		this.nombreProveedor = value;
	}
	
	/**
	 * Nombre Proveedor
	 */
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	
	/**
	 * Direccion Proveedor
	 */
	public void setDireccionProveedor(String value) {
		this.direccionProveedor = value;
	}
	
	/**
	 * Direccion Proveedor
	 */
	public String getDireccionProveedor() {
		return direccionProveedor;
	}
	
	/**
	 * Activo
	 */
	public void setActivo(String value) {
		this.activo = value;
	}
	
	/**
	 * Activo
	 */
	public String getActivo() {
		return activo;
	}
	
	/**
	 * ID Bodega
	 */
	public void setIdBodega(String value) {
		this.idBodega = value;
	}
	
	/**
	 * ID Bodega
	 */
	public String getIdBodega() {
		return idBodega;
	}
	
	/**
	 * Nombre Bodega
	 */
	public void setNombreBodega(String value) {
		this.nombreBodega = value;
	}
	
	/**
	 * Nombre Bodega
	 */
	public String getNombreBodega() {
		return nombreBodega;
	}
	
	public String toString() {
		return String.valueOf(getIdArticulo());
	}
	
}
