/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ArticuloCriteria extends AbstractORMCriteria {
	public final IntegerExpression idArticulo;
	public final StringExpression nombreArticulo;
	public final StringExpression descripcionArticulo;
	public final StringExpression precioCompra;
	public final StringExpression valorAprox;
	public final StringExpression idProveedor;
	public final StringExpression nombreProveedor;
	public final StringExpression direccionProveedor;
	public final StringExpression activo;
	public final StringExpression idBodega;
	public final StringExpression nombreBodega;
	
	public ArticuloCriteria(Criteria criteria) {
		super(criteria);
		idArticulo = new IntegerExpression("idArticulo", this);
		nombreArticulo = new StringExpression("nombreArticulo", this);
		descripcionArticulo = new StringExpression("descripcionArticulo", this);
		precioCompra = new StringExpression("precioCompra", this);
		valorAprox = new StringExpression("valorAprox", this);
		idProveedor = new StringExpression("idProveedor", this);
		nombreProveedor = new StringExpression("nombreProveedor", this);
		direccionProveedor = new StringExpression("direccionProveedor", this);
		activo = new StringExpression("activo", this);
		idBodega = new StringExpression("idBodega", this);
		nombreBodega = new StringExpression("nombreBodega", this);
	}
	
	public ArticuloCriteria(PersistentSession session) {
		this(session.createCriteria(Articulo.class));
	}
	
	public ArticuloCriteria() throws PersistentException {
		this(orm.ProyectoProgramacionPersistentManager.instance().getSession());
	}
	
	public Articulo uniqueArticulo() {
		return (Articulo) super.uniqueResult();
	}
	
	public Articulo[] listArticulo() {
		java.util.List list = super.list();
		return (Articulo[]) list.toArray(new Articulo[list.size()]);
	}
}

