/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class ArticuloDAO {
	public static Articulo loadArticuloByORMID(int idArticulo) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return loadArticuloByORMID(session, idArticulo);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo getArticuloByORMID(int idArticulo) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return getArticuloByORMID(session, idArticulo);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo loadArticuloByORMID(int idArticulo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return loadArticuloByORMID(session, idArticulo, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo getArticuloByORMID(int idArticulo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return getArticuloByORMID(session, idArticulo, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo loadArticuloByORMID(PersistentSession session, int idArticulo) throws PersistentException {
		try {
			return (Articulo) session.load(orm.Articulo.class, new Integer(idArticulo));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo getArticuloByORMID(PersistentSession session, int idArticulo) throws PersistentException {
		try {
			return (Articulo) session.get(orm.Articulo.class, new Integer(idArticulo));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo loadArticuloByORMID(PersistentSession session, int idArticulo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Articulo) session.load(orm.Articulo.class, new Integer(idArticulo), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo getArticuloByORMID(PersistentSession session, int idArticulo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Articulo) session.get(orm.Articulo.class, new Integer(idArticulo), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryArticulo(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return queryArticulo(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryArticulo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return queryArticulo(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo[] listArticuloByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return listArticuloByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo[] listArticuloByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return listArticuloByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryArticulo(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Articulo as Articulo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryArticulo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Articulo as Articulo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Articulo", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo[] listArticuloByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryArticulo(session, condition, orderBy);
			return (Articulo[]) list.toArray(new Articulo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo[] listArticuloByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryArticulo(session, condition, orderBy, lockMode);
			return (Articulo[]) list.toArray(new Articulo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo loadArticuloByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return loadArticuloByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo loadArticuloByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return loadArticuloByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo loadArticuloByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Articulo[] articulos = listArticuloByQuery(session, condition, orderBy);
		if (articulos != null && articulos.length > 0)
			return articulos[0];
		else
			return null;
	}
	
	public static Articulo loadArticuloByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Articulo[] articulos = listArticuloByQuery(session, condition, orderBy, lockMode);
		if (articulos != null && articulos.length > 0)
			return articulos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateArticuloByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return iterateArticuloByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateArticuloByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ProyectoProgramacionPersistentManager.instance().getSession();
			return iterateArticuloByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateArticuloByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Articulo as Articulo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateArticuloByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Articulo as Articulo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Articulo", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo createArticulo() {
		return new orm.Articulo();
	}
	
	public static boolean save(orm.Articulo articulo) throws PersistentException {
		try {
			orm.ProyectoProgramacionPersistentManager.instance().saveObject(articulo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Articulo articulo) throws PersistentException {
		try {
			orm.ProyectoProgramacionPersistentManager.instance().deleteObject(articulo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Articulo articulo) throws PersistentException {
		try {
			orm.ProyectoProgramacionPersistentManager.instance().getSession().refresh(articulo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Articulo articulo) throws PersistentException {
		try {
			orm.ProyectoProgramacionPersistentManager.instance().getSession().evict(articulo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Articulo loadArticuloByCriteria(ArticuloCriteria articuloCriteria) {
		Articulo[] articulos = listArticuloByCriteria(articuloCriteria);
		if(articulos == null || articulos.length == 0) {
			return null;
		}
		return articulos[0];
	}
	
	public static Articulo[] listArticuloByCriteria(ArticuloCriteria articuloCriteria) {
		return articuloCriteria.listArticulo();
	}
}
