package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import webservice.ServicioArticulo;

/**
 * Servlet implementation class ModificarArticulo
 */
public class ModificarArticulo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarArticulo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ServicioArticulo sa = new ServicioArticulo();
		String idArticulo = request.getParameter("idArticulo");
		String nombreArticulo = request.getParameter("nombreArticulo");
		String descripcionArticulo = request.getParameter("descripcionArticulo");
		String precioCompra = request.getParameter("precioCompra");
		String valorAprox = request.getParameter("valorAprox");
		String idProveedor = request.getParameter("idProveedor");
		String nombreProveedor = request.getParameter("nombreProveedor");
		String direccionProveedor = request.getParameter("direccionProveedor");
		String activo = request.getParameter("activo");
		String idBodega = request.getParameter("idBodega");
		String nombreBodega = request.getParameter("nombreBodega");
		
        sa.modificarArticulo(new domain.Articulo(nombreArticulo, descripcionArticulo, precioCompra, valorAprox, idProveedor, nombreProveedor, direccionProveedor, activo, idBodega, nombreBodega),Integer.parseInt( idArticulo));
        request.getRequestDispatcher("/ModificarArticulo.jsp").forward(request, response);
        
        
        
        
        
        
        
        
        
	}

}
