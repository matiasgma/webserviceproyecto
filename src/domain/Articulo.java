package domain;

/**
 * Clase del objeto Art�culo
 * @author Mat�as
 * @version 1.0
 * 
 */

public class Articulo {
	
	private String nombreArticulo;
	private String descripcionArticulo;
	private String precioCompra;
	private String valorAprox;
	private String idProveedor;
	private String nombreProveedor;
	private String direccionProveedor;
	private String activo;
	private String idBodega;
	private String nombreBodega;
	public Articulo(String nombreArticulo, String descripcionArticulo, String precioCompra, String valorAprox,
			String idProveedor, String nombreProveedor, String direccionProveedor, String activo, String idBodega,
			String nombreBodega) {
		
		this.nombreArticulo = nombreArticulo;
		this.descripcionArticulo = descripcionArticulo;
		this.precioCompra = precioCompra;
		this.valorAprox = valorAprox;
		this.idProveedor = idProveedor;
		this.nombreProveedor = nombreProveedor;
		this.direccionProveedor = direccionProveedor;
		this.activo = activo;
		this.idBodega = idBodega;
		this.nombreBodega = nombreBodega;
	}
	
	public  Articulo (){}

	public String getNombreArticulo() {
		return nombreArticulo;
	}

	public void setNombreArticulo(String nombreArticulo) {
		this.nombreArticulo = nombreArticulo;
	}

	public String getDescripcionArticulo() {
		return descripcionArticulo;
	}

	public void setDescripcionArticulo(String descripcionArticulo) {
		this.descripcionArticulo = descripcionArticulo;
	}

	public String getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(String precioCompra) {
		this.precioCompra = precioCompra;
	}

	public String getValorAprox() {
		return valorAprox;
	}

	public void setValorAprox(String valorAprox) {
		this.valorAprox = valorAprox;
	}

	public String getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public String getDireccionProveedor() {
		return direccionProveedor;
	}

	public void setDireccionProveedor(String direccionProveedor) {
		this.direccionProveedor = direccionProveedor;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getIdBodega() {
		return idBodega;
	}

	public void setIdBodega(String idBodega) {
		this.idBodega = idBodega;
	}

	public String getNombreBodega() {
		return nombreBodega;
	}

	public void setNombreBodega(String nombreBodega) {
		this.nombreBodega = nombreBodega;
	}

}
