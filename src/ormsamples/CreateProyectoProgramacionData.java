/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class CreateProyectoProgramacionData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = orm.ProyectoProgramacionPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Articulo lormArticulo = orm.ArticuloDAO.createArticulo();
			// Initialize the properties of the persistent object here
			orm.ArticuloDAO.save(lormArticulo);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateProyectoProgramacionData createProyectoProgramacionData = new CreateProyectoProgramacionData();
			try {
				createProyectoProgramacionData.createTestData();
			}
			finally {
				orm.ProyectoProgramacionPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
