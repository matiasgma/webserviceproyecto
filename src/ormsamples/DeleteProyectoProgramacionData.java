/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class DeleteProyectoProgramacionData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = orm.ProyectoProgramacionPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Articulo lormArticulo = orm.ArticuloDAO.loadArticuloByQuery(null, null);
			// Delete the persistent object
			orm.ArticuloDAO.delete(lormArticulo);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteProyectoProgramacionData deleteProyectoProgramacionData = new DeleteProyectoProgramacionData();
			try {
				deleteProyectoProgramacionData.deleteTestData();
			}
			finally {
				orm.ProyectoProgramacionPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
