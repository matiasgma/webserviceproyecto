/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class ListProyectoProgramacionData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Articulo...");
		orm.Articulo[] ormArticulos = orm.ArticuloDAO.listArticuloByQuery(null, null);
		int length = Math.min(ormArticulos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormArticulos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException {
		System.out.println("Listing Articulo by Criteria...");
		orm.ArticuloCriteria lormArticuloCriteria = new orm.ArticuloCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormArticuloCriteria.idArticulo.eq();
		lormArticuloCriteria.setMaxResults(ROW_COUNT);
		orm.Articulo[] ormArticulos = lormArticuloCriteria.listArticulo();
		int length =ormArticulos== null ? 0 : Math.min(ormArticulos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormArticulos[i]);
		}
		System.out.println(length + " Articulo record(s) retrieved."); 
		
	}
	
	public static void main(String[] args) {
		try {
			ListProyectoProgramacionData listProyectoProgramacionData = new ListProyectoProgramacionData();
			try {
				listProyectoProgramacionData.listTestData();
				//listProyectoProgramacionData.listByCriteria();
			}
			finally {
				orm.ProyectoProgramacionPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
