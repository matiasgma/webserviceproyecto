/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateProyectoProgramacionData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = orm.ProyectoProgramacionPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Articulo lormArticulo = orm.ArticuloDAO.loadArticuloByQuery(null, null);
			// Update the properties of the persistent object
			orm.ArticuloDAO.save(lormArticulo);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving Articulo by ArticuloCriteria");
		orm.ArticuloCriteria lormArticuloCriteria = new orm.ArticuloCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormArticuloCriteria.idArticulo.eq();
		System.out.println(lormArticuloCriteria.uniqueArticulo());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateProyectoProgramacionData retrieveAndUpdateProyectoProgramacionData = new RetrieveAndUpdateProyectoProgramacionData();
			try {
				retrieveAndUpdateProyectoProgramacionData.retrieveAndUpdateTestData();
				//retrieveAndUpdateProyectoProgramacionData.retrieveByCriteria();
			}
			finally {
				orm.ProyectoProgramacionPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
