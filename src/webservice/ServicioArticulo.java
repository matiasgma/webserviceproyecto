package webservice;
/**
 * Clase que contiene los servicios del proyecto
 * @author Mat�as
 * @version 1.0
 * 
 */

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class ServicioArticulo {
	
	/**
	* M�todo que captura los atributos de un Art�culo 
	* @param oArticulo
	* @return
	*/
	public String agregarArticulo(domain.Articulo oArticulo){
	PersistentTransaction t;
	try {
	t = orm.ProyectoProgramacionPersistentManager.instance().getSession().beginTransaction();
	try {
	orm.Articulo lormArticulo = orm.ArticuloDAO.createArticulo();
	// Initialize the properties of the persistent object here
	lormArticulo.setNombreArticulo(oArticulo.getNombreArticulo());
	lormArticulo.setDescripcionArticulo(oArticulo.getDescripcionArticulo());
	lormArticulo.setPrecioCompra(oArticulo.getPrecioCompra());
	lormArticulo.setValorAprox(oArticulo.getValorAprox());
	lormArticulo.setIdProveedor(oArticulo.getIdProveedor());
	lormArticulo.setNombreProveedor(oArticulo.getNombreProveedor());
	lormArticulo.setDireccionProveedor(oArticulo.getDireccionProveedor());
	lormArticulo.setActivo(oArticulo.getActivo());
	lormArticulo.setIdBodega(oArticulo.getIdBodega());
	lormArticulo.setNombreBodega(oArticulo.getNombreBodega());
	
	
	System.out.println("Ingreso Exitoso");
	orm.ArticuloDAO.save(lormArticulo);
	t.commit();
	return "ingreso existoso";
	}
	catch (Exception e) {
	t.rollback();
	return "Error-Rollback";
	}
	} catch (PersistentException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
	return "Error persistencia";
	}
	}
	
	/**
	* M�todo que etorna un listado de objeto de la clase Articulo
	* @return List<domain.Articulo>
	*/
	public List<domain.Articulo> mostrarArticulo(){
	List<domain.Articulo> articulos = new
	ArrayList<domain.Articulo>();
	orm.Articulo[] ormArticulos;
	try {
	ormArticulos = orm.ArticuloDAO.listArticuloByQuery(null, null);
	int length = ormArticulos.length;
	for (int i = 0; i < length; i++) {
	System.out.println(ormArticulos[i]);
	articulos.add(new
	domain.Articulo(ormArticulos[i].getNombreArticulo(),
	ormArticulos[i].getDescripcionArticulo(),
	ormArticulos[i].getPrecioCompra(),
	ormArticulos[i].getValorAprox(),
	ormArticulos[i].getIdProveedor(),
	ormArticulos[i].getNombreProveedor(),
	ormArticulos[i].getDireccionProveedor(),
	ormArticulos[i].getActivo(),
	ormArticulos[i].getIdBodega(),
	ormArticulos[i].getNombreBodega()));
	
	}
	return articulos;
	} catch (PersistentException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	return null;
	}
	}
	/**
	 * M�todo que elimina un objeto Art�culo a trav�s de su id
	 * @param idArticulo
	 * @return 
	 */
	public String eliminarArticulo(int idArticulo) {
		
		
		orm.Articulo[] ormArticulos;
		try {
		ormArticulos = orm.ArticuloDAO.listArticuloByQuery(null, null);
		int length = ormArticulos.length;
		for (int i = 0; i < length; i++) {
			if(idArticulo == ormArticulos[i].getIdArticulo()){
				
				orm.ArticuloDAO.delete(ormArticulos[i]);
				return  "Eliminaci�n Exitosa";
				 
			}
	
		}
	}catch(Exception e){
		return "error";
	}
		return null;
}
	
	/**
	 * M�todo que modifica los atributos de un objeto ingresado
	 * @param oArticulo
	 * @param idArticulo
	 * @return
	 */
    public String modificarArticulo(domain.Articulo oArticulo,int idArticulo) {
		
		orm.Articulo[] ormArticulos;
		try {
		ormArticulos = orm.ArticuloDAO.listArticuloByQuery(null, null);
		int length = ormArticulos.length;
		for (int i = 0; i < length; i++) {
			
			if(idArticulo == ormArticulos[i].getIdArticulo()){ 
				
				ormArticulos[i].setNombreArticulo(oArticulo.getNombreArticulo());
				ormArticulos[i].setDescripcionArticulo(oArticulo.getDescripcionArticulo());
				ormArticulos[i].setPrecioCompra(oArticulo.getPrecioCompra());
				ormArticulos[i].setValorAprox(oArticulo.getValorAprox());
				ormArticulos[i].setIdProveedor(oArticulo.getIdProveedor());
				ormArticulos[i].setNombreProveedor(oArticulo.getIdProveedor());
				ormArticulos[i].setDireccionProveedor(oArticulo.getDireccionProveedor());
				ormArticulos[i].setActivo(oArticulo.getActivo());
				ormArticulos[i].setIdBodega(oArticulo.getIdBodega());
				ormArticulos[i].setNombreBodega(oArticulo.getNombreBodega());
				
				
				orm.ArticuloDAO.save(ormArticulos[i]);
				
				return  "Modificaci�n Exitosa";
			}
		}
		
		}catch(Exception e){
			return "error";
		
	}
		return null;
	}

}
