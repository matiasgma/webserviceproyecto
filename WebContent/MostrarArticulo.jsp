<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="MostrarArticulo" method="post">
<input type="submit" value="Mostrar" >
<br>
<i:forEach items="${listaArticulo}"
var="articulo">
${articulo.nombreArticulo} : ${articulo.descripcionArticulo} : ${articulo.precioCompra} : ${articulo.valorAprox} : ${articulo.idProveedor} : ${articulo.nombreProveedor} : ${articulo.direccionProveedor} : ${articulo.activo} : ${articulo.idBodega} : ${articulo.nombreBodega}
<br/>
</i:forEach>
</form>
</body>
</html>